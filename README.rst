========
CLI-GPAW
========

Command-line interface to GPAW.

.. contents::

Subcommands
===========

.. computer generated text:


Build atomic-structure (build)
------------------------------

usage: cg build [-h] [--pbc PBC] [-M MAGMOMS] [-V VACUUM] [-r REPEAT]
                [-d LENGTH]
                atoms [output]

Recipes:

* mol:name
* atom:symbol
* dimer:name
* fcc:symbol,celltype,a
* bcc:symbol,celltype,a
* diamond:symbol,celltype,a
* zincblende:symbols,celltype,a
* mx2:formula,kind,a,thickness
* graphene:formula,a

atoms:
    Name of a file to read atoms from or a build-recipe like "recipe:args", where "recipe" must be one of "mol", "atom", "dimer", "fcc", "bcc", "diamond", "zincblende", "mx2", "graphene" and "args" are arguments to the recipe. Examples: "mol:H2O", "bcc:Fe", "diamond:Si,a=5.4".
output:
    Output file (defaults to stdout). Examples: "abc.xyz", "x.traj".

options:
  -h, --help            show this help message and exit
  --pbc PBC             Set periodic boundary conditions. Examples: "--pbc
                        1,1,0" (slab), "--pbc=0" (molecule).
  -M MAGMOMS, --magmoms MAGMOMS
                        Magnetic moments. Use "-M 1" or "-M 2.3,-2.3"
  -V VACUUM, --vacuum VACUUM
                        Amount of vacuum to add along non-periodic
                        direction(s) (in Å)
  -r REPEAT, --repeat REPEAT
                        Repeat unit cell. Use "-r 2" or "-r 2,3,1"
  -d LENGTH, --bond-length LENGTH
                        Bond length in Å


Do a scf calculation (scf)
--------------------------

usage: cg scf [-h] [-v] [-B] [-n NAME] [-x PATH] [-k KPTS] [-m MODE]
              [-H HUBBARD] [-e ECUT] [-s SMEARING] [--dftd3] [-p PARAMETERS]
              [--profile] [-P NCORES] [--use-old-gpaw] [--debug] [-G GPW]
              [--pot-path POT_PATH] [--after AFTER] [--properties PROPERTIES]
              [atoms]

Example:

Calculate energy and forces for H2-molecule and write result to h2.txt and
h2.json files:

  $ cg build mol:H2 | cg scf -n h2 --properties=f

atoms:
    File to read atoms from. Default is to read from stdin in xyz-format.

options:
  -h, --help            show this help message and exit
  -v, --verbose         Verbose output.
  -B, --progress-bar    Show a progress bar.
  -n NAME, --name NAME  Name of filenames.
  -x PATH, --args PATH  Replace "-x path" with options read from file given by
                        path or path/params.cg if path is a folder.
  -k KPTS, --kpts KPTS  Number of k-points. Can be three integers (giving the
                        size of the Monkhorst-Pack grid directly) or a
                        floating point number (a distance in Å), where the
                        grid size will be chosen so that the size of the
                        corresponding supercell will be at least that
                        distance. Examples: "-k1,1,1" (Γ-point only),
                        "-k6,6,6" (a 6x6x6 MP-grid), "-k 15.0".
  -m MODE, --mode MODE  Calculation mode: "pw", "lcao", "fd" or "emt". Default
                        is PW-mode.
  -H HUBBARD, --hubbard HUBBARD
                        Add Hubbard-U correction.
  -e ECUT, --ecut ECUT  Plave-wave cutoff energy in eV.
  -s SMEARING, --smearing SMEARING
                        Examples: "fd:0.1" is Fermi-dirac distribution with
                        kT=0.1 eV. The default is "cs:0.2": cold-smearing
                        (Marzari-Vanderbilt) with a width of 0.2 eV.
  --dftd3               Use DFTD3 correction.
  -p PARAMETERS, --parameters PARAMETERS
                        Comma-separated key=value pairs of calculator specific
                        parameters.
  --profile             Run through cProfile module and produce
                        "<name>-<rank>.pstat" file(s).
  -P NCORES, --parallel NCORES
                        Run in parallel on NCORES cores.
  --use-old-gpaw        Use legacy GPAW.
  --debug               Run in debug-mode.
  -G GPW, --gpw GPW     Write gpw-file when done. Use -G <name>.gpw+ to also
                        include the wave functions.
  --pot-path POT_PATH   Path to PAW-potentials.
  --after AFTER         Perform operation after calculation. Example:
                        --after="atoms.calc.write(...)"
  --properties PROPERTIES
                        Calculate extra properties (default is to calculate
                        the energy only). Use "f" for forces, "s" for stress
                        tensor and "fs" for both.


Relax atoms and/or unit cell (relax)
------------------------------------

usage: cg relax [-h] [-F MAXIMUM_FORCE] [-S MAXIMUM_STRESS] [--smask SMASK]
                [-B] [-n NAME] [-x PATH] [-k KPTS] [-m MODE] [-H HUBBARD]
                [-e ECUT] [-s SMEARING] [--dftd3] [-p PARAMETERS] [--profile]
                [-P NCORES] [--use-old-gpaw] [--debug] [-G GPW]
                [--pot-path POT_PATH] [--after AFTER]
                [atoms]

Example:

  $ cg build diamond:Si,a=5.5 | cg relax -S 0.01

atoms:
    File to read atoms from. Default is to read from stdin in xyz-format.

options:
  -h, --help            show this help message and exit
  -F MAXIMUM_FORCE, --maximum-force MAXIMUM_FORCE
                        Relax internal coordinates.
  -S MAXIMUM_STRESS, --maximum-stress MAXIMUM_STRESS
                        Relax unit-cell and internal coordinates.
  --smask SMASK         Ignore stress tensor components (xx, yy, zz, zy, zx,
                        yx). Examples: "--smask=111000" (ignore shear), "--
                        smask=110001" (slab).
  -B, --progress-bar    Show a progress bar.
  -n NAME, --name NAME  Name of filenames.
  -x PATH, --args PATH  Replace "-x path" with options read from file given by
                        path or path/params.cg if path is a folder.
  -k KPTS, --kpts KPTS  Number of k-points. Can be three integers (giving the
                        size of the Monkhorst-Pack grid directly) or a
                        floating point number (a distance in Å), where the
                        grid size will be chosen so that the size of the
                        corresponding supercell will be at least that
                        distance. Examples: "-k1,1,1" (Γ-point only),
                        "-k6,6,6" (a 6x6x6 MP-grid), "-k 15.0".
  -m MODE, --mode MODE  Calculation mode: "pw", "lcao", "fd" or "emt". Default
                        is PW-mode.
  -H HUBBARD, --hubbard HUBBARD
                        Add Hubbard-U correction.
  -e ECUT, --ecut ECUT  Plave-wave cutoff energy in eV.
  -s SMEARING, --smearing SMEARING
                        Examples: "fd:0.1" is Fermi-dirac distribution with
                        kT=0.1 eV. The default is "cs:0.2": cold-smearing
                        (Marzari-Vanderbilt) with a width of 0.2 eV.
  --dftd3               Use DFTD3 correction.
  -p PARAMETERS, --parameters PARAMETERS
                        Comma-separated key=value pairs of calculator specific
                        parameters.
  --profile             Run through cProfile module and produce
                        "<name>-<rank>.pstat" file(s).
  -P NCORES, --parallel NCORES
                        Run in parallel on NCORES cores.
  --use-old-gpaw        Use legacy GPAW.
  --debug               Run in debug-mode.
  -G GPW, --gpw GPW     Write gpw-file when done. Use -G <name>.gpw+ to also
                        include the wave functions.
  --pot-path POT_PATH   Path to PAW-potentials.
  --after AFTER         Perform operation after calculation. Example:
                        --after="atoms.calc.write(...)"


Equation of state fit (eos)
---------------------------

usage: cg eos [-h] [--npoints NPOINTS] [--max-strain MAX_STRAIN] [-v] [-B]
              [-n NAME] [-x PATH] [-k KPTS] [-m MODE] [-H HUBBARD] [-e ECUT]
              [-s SMEARING] [--dftd3] [-p PARAMETERS] [--profile] [-P NCORES]
              [--use-old-gpaw] [--debug] [-G GPW] [--pot-path POT_PATH]
              [--after AFTER]
              [atoms]

Uses Birch-Murnaghan equation.  Example:

  $ cg build fcc:Al al.xyz
  $ cg eos al.xyz -m emt --max-strain=0.02 --npoints=5 -v

atoms:
    File to read atoms from. Default is to read from stdin in xyz-format.

options:
  -h, --help            show this help message and exit
  --npoints NPOINTS     Number of points (defaults to 7).
  --max-strain MAX_STRAIN
                        Maximum strain. Default is "--max-strain=0.02" meaning
                        that lattice vectors are varied from -2 to +2 %.
  -v, --verbose         Verbose output.
  -B, --progress-bar    Show a progress bar.
  -n NAME, --name NAME  Name of filenames.
  -x PATH, --args PATH  Replace "-x path" with options read from file given by
                        path or path/params.cg if path is a folder.
  -k KPTS, --kpts KPTS  Number of k-points. Can be three integers (giving the
                        size of the Monkhorst-Pack grid directly) or a
                        floating point number (a distance in Å), where the
                        grid size will be chosen so that the size of the
                        corresponding supercell will be at least that
                        distance. Examples: "-k1,1,1" (Γ-point only),
                        "-k6,6,6" (a 6x6x6 MP-grid), "-k 15.0".
  -m MODE, --mode MODE  Calculation mode: "pw", "lcao", "fd" or "emt". Default
                        is PW-mode.
  -H HUBBARD, --hubbard HUBBARD
                        Add Hubbard-U correction.
  -e ECUT, --ecut ECUT  Plave-wave cutoff energy in eV.
  -s SMEARING, --smearing SMEARING
                        Examples: "fd:0.1" is Fermi-dirac distribution with
                        kT=0.1 eV. The default is "cs:0.2": cold-smearing
                        (Marzari-Vanderbilt) with a width of 0.2 eV.
  --dftd3               Use DFTD3 correction.
  -p PARAMETERS, --parameters PARAMETERS
                        Comma-separated key=value pairs of calculator specific
                        parameters.
  --profile             Run through cProfile module and produce
                        "<name>-<rank>.pstat" file(s).
  -P NCORES, --parallel NCORES
                        Run in parallel on NCORES cores.
  --use-old-gpaw        Use legacy GPAW.
  --debug               Run in debug-mode.
  -G GPW, --gpw GPW     Write gpw-file when done. Use -G <name>.gpw+ to also
                        include the wave functions.
  --pot-path POT_PATH   Path to PAW-potentials.
  --after AFTER         Perform operation after calculation. Example:
                        --after="atoms.calc.write(...)"


Egg-box error test (eggbox)
---------------------------

usage: cg eggbox [-h] [--npoints NPOINTS] [-v] [-B] [-n NAME] [-x PATH]
                 [-k KPTS] [-m MODE] [-H HUBBARD] [-e ECUT] [-s SMEARING]
                 [--dftd3] [-p PARAMETERS] [--profile] [-P NCORES]
                 [--use-old-gpaw] [--debug] [-G GPW] [--pot-path POT_PATH]
                 [--after AFTER]
                 [atoms]

Example:

  $ cg build atom:Li --pbc=1 -V 2.0 li.xyz
  $ cg eggbox li.xyz --npoints=21

atoms:
    File to read atoms from. Default is to read from stdin in xyz-format.

options:
  -h, --help            show this help message and exit
  --npoints NPOINTS     Number of points (defaults to 31).
  -v, --verbose         Verbose output.
  -B, --progress-bar    Show a progress bar.
  -n NAME, --name NAME  Name of filenames.
  -x PATH, --args PATH  Replace "-x path" with options read from file given by
                        path or path/params.cg if path is a folder.
  -k KPTS, --kpts KPTS  Number of k-points. Can be three integers (giving the
                        size of the Monkhorst-Pack grid directly) or a
                        floating point number (a distance in Å), where the
                        grid size will be chosen so that the size of the
                        corresponding supercell will be at least that
                        distance. Examples: "-k1,1,1" (Γ-point only),
                        "-k6,6,6" (a 6x6x6 MP-grid), "-k 15.0".
  -m MODE, --mode MODE  Calculation mode: "pw", "lcao", "fd" or "emt". Default
                        is PW-mode.
  -H HUBBARD, --hubbard HUBBARD
                        Add Hubbard-U correction.
  -e ECUT, --ecut ECUT  Plave-wave cutoff energy in eV.
  -s SMEARING, --smearing SMEARING
                        Examples: "fd:0.1" is Fermi-dirac distribution with
                        kT=0.1 eV. The default is "cs:0.2": cold-smearing
                        (Marzari-Vanderbilt) with a width of 0.2 eV.
  --dftd3               Use DFTD3 correction.
  -p PARAMETERS, --parameters PARAMETERS
                        Comma-separated key=value pairs of calculator specific
                        parameters.
  --profile             Run through cProfile module and produce
                        "<name>-<rank>.pstat" file(s).
  -P NCORES, --parallel NCORES
                        Run in parallel on NCORES cores.
  --use-old-gpaw        Use legacy GPAW.
  --debug               Run in debug-mode.
  -G GPW, --gpw GPW     Write gpw-file when done. Use -G <name>.gpw+ to also
                        include the wave functions.
  --pot-path POT_PATH   Path to PAW-potentials.
  --after AFTER         Perform operation after calculation. Example:
                        --after="atoms.calc.write(...)"


Bader analysis (bader)
----------------------

usage: cg bader [-h] gpw_file

...

gpw_file:
    

options:
  -h, --help  show this help message and exit


Do a fixed-density calculation (fixed-density)
----------------------------------------------

usage: cg fixed-density [-h] [-B] [-n NAME] [-x PATH] [-k KPTS] [-m MODE]
                        [-H HUBBARD] [-e ECUT] [-s SMEARING] [--dftd3]
                        [-p PARAMETERS] [--profile] [-P NCORES]
                        [--use-old-gpaw] [--debug] [-G GPW]
                        [--pot-path POT_PATH] [--after AFTER]
                        gpw_file

More bands and more k-points:

  $ cg build diamond:Si,a=5.4 | cg scf -G si1.gpw
  $ cg fixed-density si1.gpw -k 8,8,8,G -p nbands=20 -G si2.gpw

gpw_file:
    

options:
  -h, --help            show this help message and exit
  -B, --progress-bar    Show a progress bar.
  -n NAME, --name NAME  Name of filenames.
  -x PATH, --args PATH  Replace "-x path" with options read from file given by
                        path or path/params.cg if path is a folder.
  -k KPTS, --kpts KPTS  Number of k-points. Can be three integers (giving the
                        size of the Monkhorst-Pack grid directly) or a
                        floating point number (a distance in Å), where the
                        grid size will be chosen so that the size of the
                        corresponding supercell will be at least that
                        distance. Examples: "-k1,1,1" (Γ-point only),
                        "-k6,6,6" (a 6x6x6 MP-grid), "-k 15.0".
  -m MODE, --mode MODE  Calculation mode: "pw", "lcao", "fd" or "emt". Default
                        is PW-mode.
  -H HUBBARD, --hubbard HUBBARD
                        Add Hubbard-U correction.
  -e ECUT, --ecut ECUT  Plave-wave cutoff energy in eV.
  -s SMEARING, --smearing SMEARING
                        Examples: "fd:0.1" is Fermi-dirac distribution with
                        kT=0.1 eV. The default is "cs:0.2": cold-smearing
                        (Marzari-Vanderbilt) with a width of 0.2 eV.
  --dftd3               Use DFTD3 correction.
  -p PARAMETERS, --parameters PARAMETERS
                        Comma-separated key=value pairs of calculator specific
                        parameters.
  --profile             Run through cProfile module and produce
                        "<name>-<rank>.pstat" file(s).
  -P NCORES, --parallel NCORES
                        Run in parallel on NCORES cores.
  --use-old-gpaw        Use legacy GPAW.
  --debug               Run in debug-mode.
  -G GPW, --gpw GPW     Write gpw-file when done. Use -G <name>.gpw+ to also
                        include the wave functions.
  --pot-path POT_PATH   Path to PAW-potentials.
  --after AFTER         Perform operation after calculation. Example:
                        --after="atoms.calc.write(...)"


Plot band-structure (band-structure)
------------------------------------

usage: cg band-structure [-h] [-q] [-B] [-n NAME] [-k KPTS] [-r RANGE]
                         gpw_file

Read eigenvalues and k-points from file and plot result from band-structure
calculation or interpolate from Monkhorst-Pack sampling to a given path
(--kpts <path>).

Example:

  $ cg build fcc:Al | cg scf -G al.gpw
  $ cg fixed-density al.gpw -k GX -G al-bs.gpw
  $ cg band-structure al-bs.gpw -r -10 10

gpw_file:
    Path to output file(s) from calculation.

options:
  -h, --help            show this help message and exit
  -q, --quiet           Less output.
  -B, --progress-bar    Show a progress bar.
  -n NAME, --name NAME  Name of filenames.
  -k KPTS, --kpts KPTS  Example "GXL".
  -r RANGE, --range RANGE
                        Example: "-r -3.0 3.0" (in eV relative to Fermi-
                        level).


Run Python interpreter (python)
-------------------------------

usage: cg python [-h] [-B] [--profile] [-P NCORES] [--use-old-gpaw] [--debug]
                 [-G GPW] [--pot-path POT_PATH] [--after AFTER]
                 [--command CMD] [--module MODULE]
                 [ARG ...]

Instead of "python script.py" you can do:

  $ cg python --progress --pot-path=. script.py

ARG:
    Arguments passed to program in sys.argv[1:]. Use -- to force all remaining arguments to be passed to target program (useful for passing options starting with -).

options:
  -h, --help            show this help message and exit
  -B, --progress-bar    Show a progress bar.
  --profile             Run through cProfile module and produce
                        "<name>-<rank>.pstat" file(s).
  -P NCORES, --parallel NCORES
                        Run in parallel on NCORES cores.
  --use-old-gpaw        Use legacy GPAW.
  --debug               Run in debug-mode.
  -G GPW, --gpw GPW     Write gpw-file when done. Use -G <name>.gpw+ to also
                        include the wave functions.
  --pot-path POT_PATH   Path to PAW-potentials.
  --after AFTER         Perform operation after calculation. Example:
                        --after="atoms.calc.write(...)"
  --command CMD, -c CMD
                        Program passed in as string (terminates option list).
  --module MODULE, -m MODULE
                        Run library module as a script (terminates option
                        list).


Enable BASH tab-completion (completion)
---------------------------------------

usage: cg completion [-h]

Do this:

  $ cg completion >> ~/.bashrc

options:
  -h, --help  show this help message and exit


ACWF-benchmark (acwf)
---------------------

usage: cg acwf [-h] [-B] [-n NAME] [-x PATH] [-k KPTS] [-m MODE] [-H HUBBARD]
               [-e ECUT] [-s SMEARING] [--dftd3] [-p PARAMETERS] [--profile]
               [-P NCORES] [--use-old-gpaw] [--debug] [-G GPW]
               [--pot-path POT_PATH] [--after AFTER]
               symbol {XO3,XO,X4O6,XO2,X4O10,X2O,Diamond,FCC,SC,BCC}

Do equation-of-state calculation for the 10 reference systems from the AiiDA
common workflows (ACWF) benchmark: DIAMOND, FCC, SC, BCC, XO3, XO, X4O6, XO2,
X4O10, X2O.

Example:

  $ cg acwf Al FCC -B

symbol:
    
{XO3,XO,X4O6,XO2,X4O10,X2O,Diamond,FCC,SC,BCC}:
    

options:
  -h, --help            show this help message and exit
  -B, --progress-bar    Show a progress bar.
  -n NAME, --name NAME  Name of filenames.
  -x PATH, --args PATH  Replace "-x path" with options read from file given by
                        path or path/params.cg if path is a folder.
  -k KPTS, --kpts KPTS  Number of k-points. Can be three integers (giving the
                        size of the Monkhorst-Pack grid directly) or a
                        floating point number (a distance in Å), where the
                        grid size will be chosen so that the size of the
                        corresponding supercell will be at least that
                        distance. Examples: "-k1,1,1" (Γ-point only),
                        "-k6,6,6" (a 6x6x6 MP-grid), "-k 15.0".
  -m MODE, --mode MODE  Calculation mode: "pw", "lcao", "fd" or "emt". Default
                        is PW-mode.
  -H HUBBARD, --hubbard HUBBARD
                        Add Hubbard-U correction.
  -e ECUT, --ecut ECUT  Plave-wave cutoff energy in eV.
  -s SMEARING, --smearing SMEARING
                        Examples: "fd:0.1" is Fermi-dirac distribution with
                        kT=0.1 eV. The default is "cs:0.2": cold-smearing
                        (Marzari-Vanderbilt) with a width of 0.2 eV.
  --dftd3               Use DFTD3 correction.
  -p PARAMETERS, --parameters PARAMETERS
                        Comma-separated key=value pairs of calculator specific
                        parameters.
  --profile             Run through cProfile module and produce
                        "<name>-<rank>.pstat" file(s).
  -P NCORES, --parallel NCORES
                        Run in parallel on NCORES cores.
  --use-old-gpaw        Use legacy GPAW.
  --debug               Run in debug-mode.
  -G GPW, --gpw GPW     Write gpw-file when done. Use -G <name>.gpw+ to also
                        include the wave functions.
  --pot-path POT_PATH   Path to PAW-potentials.
  --after AFTER         Perform operation after calculation. Example:
                        --after="atoms.calc.write(...)"
