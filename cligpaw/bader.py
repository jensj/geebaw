"""Bader charge analysis."""
import subprocess
from pathlib import Path

import numpy as np
from ase import Atoms
from ase.io import write
from ase.units import Bohr


def bader(atoms: Atoms) -> np.ndarray:
    """Preform Bader analysis.

    * calculate all-electron density
    * write CUBE file
    * run "bader" program
    * check for correct number of volumes
    * check for correct total charge

    Returns ASE Atoms object and ndarray of charges in units of :math:`|e|`.
    """
    from gpaw.mpi import world
    assert world.size == 1, 'Do not run in parallel!'
    assert np.linalg.det(atoms.cell) > 0.0
    rho = atoms.calc.get_all_electron_density(gridrefinement=4)
    write('density.cube', atoms, data=rho * Bohr**3)
    subprocess.run(['bader', 'density.cube'])
    charges = -read_bader_charges('ACF.dat')
    charges += atoms.get_atomic_numbers()
    assert abs(charges.sum()) < 0.01
    return charges


def read_bader_charges(filename: str | Path = 'ACF.dat') -> np.ndarray:
    path = Path(filename)
    charges = []
    with path.open() as fd:
        for line in fd:
            words = line.split()
            if len(words) == 7:
                charges.append(float(words[4]))
    return np.array(charges)
