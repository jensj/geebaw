import argparse
import shlex
import sys
import textwrap
from pathlib import Path
from typing import Any


class MyArgumentParser:
    def __init__(self, prog, description) -> None:
        self.parser = argparse.ArgumentParser(
            prog=prog,
            description=description,
            formatter_class=Formatter,
            allow_abbrev=False)
        self.options: dict[str, int] = {}
        self.subparsers = self.parser.add_subparsers(title='Commands',
                                                     dest='command')
        self.add_parser('help').add_argument('cmd', nargs='?')

    def add_parser(self, *args, **kwargs):
        return MySubParser(
            self.subparsers.add_parser(
                *args, **kwargs,
                formatter_class=Formatter),
            self.options)

    def parse_args(self, argv):
        if isinstance(argv, str):
            argv = shlex.split(argv)
        elif argv is None:
            argv = sys.argv[1:]
        for i, arg in enumerate(argv):
            if arg.startswith(('-x', '--args')):
                if arg in {'-x', '--args'}:
                    path = Path(argv[i + 1])
                    del argv[i:i + 2]
                elif arg.startswith('-x'):
                    path = Path(argv[i][2:])
                    del argv[i:i + 1]
                elif arg.startswith('--args='):
                    path = Path(argv[i][7:])
                    del argv[i:i + 1]
                else:
                    raise ValueError
                if path.is_dir():
                    path = path / 'params.cg'
                argv[i:i] = shlex.split(path.read_text().replace('\n', ' '))
        args = self.parser.parse_args(self.fix_option_order(argv))
        return args

    def fix_option_order(self,
                         arguments: list[str]) -> list[str]:
        """Allow intermixed options and arguments."""
        args1: list[str] = []
        args2: list[str] = []
        i = 0
        while i < len(arguments):
            a = arguments[i]
            if a == '--':
                args2 += arguments[i:]
                break
            if a == '--version':
                args1.append(a)
            elif a in self.options:
                n = self.options[a]
                args2 += arguments[i:i + 1 + n]
                i += n
            elif a.startswith('--') and '=' in a:
                args2.append(a)
            elif a.startswith('-'):
                for j, b in enumerate(a[1:]):
                    n = self.options.get('-' + b, 0)
                    if n:
                        if j < len(a) - 2:
                            n = 0
                        args2 += arguments[i:i + 1 + n]
                        i += n
                        break
                else:
                    args2.append(a)
            else:
                args1.append(a)
            i += 1
        return args1 + args2


class MySubParser:
    def __init__(self, parser, options):
        self.parser = parser
        self.options = options

    def add_argument(self, *args: str, **kwargs: Any) -> None:
        """Wrapper for Parser.add_argument().

        Hack to fix argparse's handling of options.  See
        fix_option_order() function below."""

        x = self.parser.add_argument(*args, **kwargs)
        if x is None:
            return
        for o in x.option_strings:
            nargs = x.nargs if x.nargs is not None else 1
            assert isinstance(nargs, int)
            self.options[o] = nargs


class Formatter(argparse.HelpFormatter):
    """Improved help formatter."""
    def _fill_text(self, text: str, width: int, indent: str) -> str:
        assert indent == ''
        out = ''
        blocks = text.split('\n\n')
        for block in blocks:
            if block[0] == '*':
                # List items:
                for item in block[2:].split('\n* '):
                    out += textwrap.fill(item,
                                         width=width - 2,
                                         initial_indent='* ',
                                         subsequent_indent='  ') + '\n'
            elif block[0] == ' ':
                # Indented literal block:
                out += block + '\n'
            else:
                # Block of text:
                out += textwrap.fill(block, width=width) + '\n'
            out += '\n'
        return out[:-1]
