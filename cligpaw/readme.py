"""Useful utilities."""
from __future__ import annotations

import os
import sys
from io import StringIO
from pathlib import Path

from cligpaw.cli import COMMAND_NAMES, main


def update_readme(test: bool = False) -> None:
    """Update README.rst."""
    fd = sys.stdout
    sys.stdout = StringIO()

    for cmd in COMMAND_NAMES:
        print(cmd, file=fd)
        main(['help', cmd])

    txt = sys.stdout.getvalue()
    sys.stdout = fd

    txt = txt.replace(':\n\n    ', '::\n\n    ')
    txt = txt.replace('EMIN EMAX', 'RANGE')
    newlines = txt.splitlines()

    for cmd in COMMAND_NAMES:
        cmd = cmd.replace('_', '-')
        print(cmd)
        for i, line in enumerate(newlines):
            if line.startswith(f'usage: cg {cmd}'):
                break
        j = i + 1
        while newlines[j] != '':
            j += 1
        title = newlines[j + 1]
        newlines[j:j + 2] = []
        assert title[-1] == '.'
        title = title[:-1] + f' ({cmd})'
        print(title)
        newlines[i:i] = ['', '', title, '-' * len(title), '']

    n = 0
    while n < len(newlines):
        line = newlines[n]
        if line == 'positional arguments:':
            L: list[str] = []
            n += 1
            while True:
                line = newlines.pop(n)
                if not line:
                    break
                if not line.startswith('                '):
                    cmd, _, help = line.strip().partition(' ')
                    L.append(f'{cmd}:\n    {help.strip()}')
                else:
                    L[-1] += ' ' + line.strip()
            newlines[n - 1:n] = L + ['']
            n += len(L)
        n += 1

    readme = Path(__file__).parent.parent / 'README.rst'

    lines = readme.read_text().splitlines()
    a = lines.index('.. computer generated text:')
    if test:
        old = '\n'.join(lines[a + 1:])
        new = '\n'.join(newlines)
        assert new == old
    else:
        lines[a + 1:] = newlines
        readme.write_text('\n'.join(lines) + '\n')


if __name__ == '__main__':
    os.environ['COLUMNS'] = '80'
    update_readme()
