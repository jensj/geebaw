from pathlib import Path
from typing import Any

from ase import Atoms
from ase.calculators.dftd3 import DFTD3
from ase.calculators.emt import EMT


def attach_calculator(atoms: Atoms,
                      params: dict[str, Any],
                      args) -> None:
    from gpaw import setup_paths
    from gpaw.calculator import GPAW as OldGPAW
    from gpaw.new.ase_interface import GPAW as NewGPAW
    from gpaw.new.ase_interface import ASECalculator

    calc: EMT | ASECalculator | OldGPAW | DFTD3
    if params['mode']['name'] == 'emt':
        calc = EMT()
    else:
        if getattr(args, 'verbose', False):
            for key, val in params.items():
                print(f'{key}: {val}')
        if args.use_old_gpaw:
            calc = OldGPAW(**params)
        else:
            calc = NewGPAW(**params)

    if args.pot_path:
        paths = [Path(path).expanduser()
                 for path in args.pot_path.strip(':').split(':')]
        if args.pot_path.startswith(':'):
            setup_paths += paths
        elif args.pot_path.endswith(':'):
            setup_paths[:0] = paths
        else:
            setup_paths[:] = paths

    if args.dftd3:
        # DFTD3 crashes on single isolated atom
        if len(atoms) > 1 or atoms.pbc.any():
            assert params['xc'] == 'PBE' or params['mode']['name'] == 'emt'
            calc = DFTD3(dft=calc, xc='PBE', command='dftd3')

    atoms.calc = calc
