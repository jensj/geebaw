from __future__ import annotations

import os
import sys
import time
from math import inf, log10
import contextlib
from typing import Callable

from ase import Atoms
from ase.calculators.calculator import PropertyNotImplementedError
from ase.filters import FrechetCellFilter
from ase.io import Trajectory
from ase.optimize import BFGS


@contextlib.contextmanager
def stdout():
    yield sys.stdout


def paropen(name, mode='r', buffering=-1, encoding=None, comm=None):
    """MPI-safe version of open function.

    In read mode, the file is opened on all nodes.  In write and
    append mode, the file is opened on the master only, and /dev/null
    is opened on all other nodes.
    """
    from gpaw.mpi import world
    if comm is None:
        comm = world
    if comm.rank > 0 and mode[0] != 'r':
        name = os.devnull
    if name == '-':
        return stdout()
    return open(name, mode, buffering, encoding)


def relax(atoms: Atoms,
          fmax: float = 0.05,
          smax: float = inf,
          smask: list[int] | None = None,
          name='relax',
          callback: Callable[[float], None] = lambda x: None) -> dict:
    if smask is None:
        #                    xx yy zz yz xz xy
        smask = {(0, 0, 0): [0, 0, 0, 0, 0, 0],
                 (1, 0, 0): [1, 0, 0, 0, 0, 0],
                 (0, 1, 0): [0, 1, 0, 0, 0, 0],
                 (0, 0, 1): [0, 0, 1, 0, 0, 0],
                 (0, 1, 1): [0, 1, 1, 1, 0, 0],
                 (1, 0, 1): [1, 0, 1, 0, 1, 0],
                 (1, 1, 0): [1, 1, 0, 0, 0, 1],
                 (1, 1, 1): [1, 1, 1, 1, 1, 1]}[tuple(atoms.pbc.astype(int))]

    if not any(smask):
        assert smax == inf

    if smax < inf:
        optimizer = BFGS(FrechetCellFilter(atoms, mask=smask),  # type: ignore
                         logfile=None)  # type: ignore
    else:
        optimizer = BFGS(atoms, logfile=None)  # type: ignore

    name = name or 'relax'
    if name == '-':
        logfilename = '-'
        name = ''
    else:
        logfilename = f'{name}.log'

    with paropen(logfilename, 'a', encoding='utf-8') as logfile:
        with Trajectory(f'{name}.traj', 'a', atoms) as trajectory:
            fmax0 = -1.0
            smax0 = -1.0
            for i, _ in enumerate(optimizer.irun(fmax=0.0)):
                energy = atoms.get_potential_energy()
                forces = atoms.get_forces()
                fmaxi = (forces**2).sum(1).max()**0.5
                if fmax0 < 0.0:
                    fmax0 = fmaxi
                if smax < inf:
                    stress = atoms.get_stress()
                    smaxi = abs(stress * smask).max()
                    if smax0 < 0.0:
                        smax0 = smaxi
                else:
                    stress = None
                    smaxi = -1.0
                try:
                    magmoms = atoms.get_magnetic_moments()
                except PropertyNotImplementedError:
                    magmoms = None
                else:
                    if not magmoms.any():
                        magmoms = None
                trajectory.write()
                text = log(atoms, i, energy, fmaxi, smaxi, magmoms)
                print(text, file=logfile, flush=True)
                fconv = conv(fmaxi, fmax0, fmax)
                sconv = conv(smaxi, smax0, smax)
                callback(min(fconv, sconv))
                if fmaxi < fmax and smaxi < smax:
                    break
                if magmoms is not None and abs(magmoms).max() < 0.01:
                    atoms.set_initial_magnetic_moments(None)
                    atoms.calc = atoms.calc.new(txt=f'{name}-nm.txt')
    result = {'iterations': i,
              'energy': energy,
              'forces': forces}
    if stress is not None:
        result['stress'] = stress
    if magmoms is not None:
        result['magmoms'] = magmoms
    return result


def conv(xmaxi, xmax0, xmax):
    if xmaxi < xmax:
        return 1.0
    return min(log10(xmax0 / xmaxi) / log10(xmax0 / xmax), 1.0)


def log(atoms: Atoms,
        i: int,
        energy: float,
        force: float,
        stress: float,
        magmoms: list[float] | None) -> str:
    if i == 0:
        header1 = '# iter   time     energy    forces'
        header2 = '#  [hh:mm:ss]       [eV]    [eV/Å]'
        if stress >= 0.0:
            header1 += '   stress     volume'
            header2 += ' [eV/Å^3]      [Å^3]'
        if magmoms is not None:
            header1 += '  magmoms (min, max, total)'
            header2 += '   [μ_B]'
        text = f'{header1}\n{header2}\n'
    else:
        text = ''

    h, m, s = time.localtime()[3:6]
    text += f'{i:<4} {h:02d}:{m:02d}:{s:02d} {energy:10.3f} {force:9.4f}'

    if stress >= 0.0:
        v = atoms.get_volume()
        text += f'{stress:9.4f} {v:10.3f}'

    if magmoms is not None:
        magmom = atoms.get_magnetic_moment()
        m1 = min(magmoms)
        m2 = max(magmoms)
        text += f'  ({m1:.2f}, {m2:.2f}, {magmom:.2f})'

    return text
