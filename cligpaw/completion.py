from __future__ import annotations

import argparse
import os
import sys
import textwrap
from pathlib import Path
from typing import Any

from cligpaw.commands.completion import completion_command


def ensure_completions() -> None:
    """Add BASH tab-completion command to activation script."""
    venv = os.environ.get('VIRTUAL_ENV')
    if not venv:
        return
    installed = Path(venv) / 'etc/cli-gpaw/completion-installed'
    if installed.is_file():
        return
    cmd = completion_command()
    activate = Path(venv) / 'bin/activate'
    with activate.open('a') as fd:
        fd.write(f'\n# Tab-completion for CLI-GPAW:\n{cmd}\n')
    installed.parent.mkdir(exist_ok=True, parents=True)
    installed.touch()
    print(f'Just added this line:\n\n  {cmd}\n\nto your {activate} script.\n',
          file=sys.stderr)


def update_completion(test: bool = False) -> None:
    """Update commands dict in complete.py.

    Run this when ever options are changed::

        python3 -m cligpaw.completion

    """
    from cligpaw.cli import main

    # Path of the complete.py script:
    filename = Path(__file__).parent / 'complete.py'

    dct: dict[str, list[str]] = {}

    class MyException(Exception):
        pass

    class Parser:
        def __init__(self, **kwargs: Any):
            pass

        def add_argument(self, *args: str, **kwargs: Any) -> None:
            pass

        def add_subparsers(self, **kwargs: Any) -> 'Parser':
            return self

        def add_parser(self, cmd: str, **kwargs: Any) -> 'Subparser':
            return Subparser(cmd)

        def parse_args(self, args: list[str] | None = None) -> None:
            raise MyException

    class Subparser:
        def __init__(self, command: str):
            self.command = command
            dct[command] = []

        def add_argument(self, *args: str, **kwargs: Any) -> None:
            dct[self.command].extend(arg for arg in args
                                     if arg.startswith('-'))

    argparse.ArgumentParser = Parser  # type: ignore
    try:
        main()
    except MyException:
        pass

    txt = 'commands = {'
    for command, opts in sorted(dct.items()):
        txt += "\n    '" + command + "':\n        ["
        txt += '\n'.join(textwrap.wrap("'" + "', '".join(opts) + "'],",
                                       width=65,
                                       break_on_hyphens=False,
                                       subsequent_indent='         '))
    txt = txt[:-1] + '}'

    lines = filename.read_text().splitlines()

    a = lines.index('# Beginning of computer generated data:')
    b = lines.index('# End of computer generated data')

    if test:
        assert '\n'.join(lines[a + 1:b]) == txt
    else:
        lines[a + 1:b] = [txt]
        filename.write_text('\n'.join(lines) + '\n')


if __name__ == '__main__':
    update_completion()
