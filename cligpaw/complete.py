#!/usr/bin/env python3
"""Bash completion.

Put this in your .bashrc::

    complete -o default -C "python3 -m cligpaw.complete" cg

"""

from __future__ import annotations
import os
import sys
from typing import Iterable, Mapping


# Beginning of computer generated data:
commands = {
    'acwf':
        ['-B', '--progress-bar', '-n', '--name', '-x', '--args', '-k',
         '--kpts', '-m', '--mode', '-H', '--hubbard', '-e',
         '--ecut', '-s', '--smearing', '--dftd3', '-p',
         '--parameters', '--profile', '-P', '--parallel',
         '--use-old-gpaw', '--debug', '-G', '--gpw',
         '--pot-path', '--after'],
    'bader':
        [''],
    'band-structure':
        ['-q', '--quiet', '-B', '--progress-bar', '-n', '--name', '-k',
         '--kpts', '-r', '--range'],
    'build':
        ['--pbc', '-M', '--magmoms', '-V', '--vacuum', '-r', '--repeat',
         '-d', '--bond-length'],
    'completion':
        [''],
    'eggbox':
        ['--npoints', '-v', '--verbose', '-B', '--progress-bar', '-n',
         '--name', '-x', '--args', '-k', '--kpts', '-m',
         '--mode', '-H', '--hubbard', '-e', '--ecut', '-s',
         '--smearing', '--dftd3', '-p', '--parameters',
         '--profile', '-P', '--parallel', '--use-old-gpaw',
         '--debug', '-G', '--gpw', '--pot-path', '--after'],
    'eos':
        ['--npoints', '--max-strain', '-v', '--verbose', '-B',
         '--progress-bar', '-n', '--name', '-x', '--args', '-k',
         '--kpts', '-m', '--mode', '-H', '--hubbard', '-e',
         '--ecut', '-s', '--smearing', '--dftd3', '-p',
         '--parameters', '--profile', '-P', '--parallel',
         '--use-old-gpaw', '--debug', '-G', '--gpw',
         '--pot-path', '--after'],
    'fixed-density':
        ['-B', '--progress-bar', '-n', '--name', '-x', '--args', '-k',
         '--kpts', '-m', '--mode', '-H', '--hubbard', '-e',
         '--ecut', '-s', '--smearing', '--dftd3', '-p',
         '--parameters', '--profile', '-P', '--parallel',
         '--use-old-gpaw', '--debug', '-G', '--gpw',
         '--pot-path', '--after'],
    'help':
        [''],
    'python':
        ['-B', '--progress-bar', '--profile', '-P', '--parallel',
         '--use-old-gpaw', '--debug', '-G', '--gpw',
         '--pot-path', '--after', '--command', '-c', '--module',
         '-m'],
    'relax':
        ['-F', '--maximum-force', '-S', '--maximum-stress', '--smask',
         '-B', '--progress-bar', '-n', '--name', '-x', '--args',
         '-k', '--kpts', '-m', '--mode', '-H', '--hubbard', '-e',
         '--ecut', '-s', '--smearing', '--dftd3', '-p',
         '--parameters', '--profile', '-P', '--parallel',
         '--use-old-gpaw', '--debug', '-G', '--gpw',
         '--pot-path', '--after'],
    'scf':
        ['-v', '--verbose', '-B', '--progress-bar', '-n', '--name', '-x',
         '--args', '-k', '--kpts', '-m', '--mode', '-H',
         '--hubbard', '-e', '--ecut', '-s', '--smearing',
         '--dftd3', '-p', '--parameters', '--profile', '-P',
         '--parallel', '--use-old-gpaw', '--debug', '-G',
         '--gpw', '--pot-path', '--after', '--properties']}
# End of computer generated data


def complete(word: str, previous: str, line: str, point: int) -> Iterable[str]:
    for w in line[:point - len(word)].strip().split()[1:]:
        if w[0].isalpha():
            if w in commands:
                command = w
                break
    else:
        opts = ['-h', '--help']
        if word[:1] == '-':
            return opts
        return list(commands) + opts

    if word[:1] == '-':
        return commands[command]

    if previous in ['-m', '--mode']:
        words = ['pw', 'emt']
        return words

    if command == 'help':
        return [cmd for cmd in commands if cmd != 'help']

    if command == 'build':
        if previous == 'cg':
            return ['mol:', 'fcc:']

    return []


def main(environ: Mapping[str, str], word: str, previous: str) -> None:
    line = environ['COMP_LINE']
    point = int(environ['COMP_POINT'])
    words = complete(word, previous, line, point)
    for w in words:
        if w.startswith(word):
            print(w)


if __name__ == '__main__':
    word, previous = sys.argv[2:]
    main(os.environ, word, previous)
