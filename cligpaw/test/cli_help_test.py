import importlib
import os
import io
from pathlib import Path

import pytest
from cligpaw.cli import COMMAND_NAMES
from cligpaw.cli import main as cg


@pytest.mark.parametrize('cmd', COMMAND_NAMES)
def test_cli_help(tmp_path, capsys, monkeypatch, cmd):
    import matplotlib.pyplot as plt
    monkeypatch.setattr(plt, 'show', lambda: None)
    os.chdir(tmp_path)
    module = importlib.import_module(f'cligpaw.commands.{cmd}')
    desc = getattr(module, 'DESCRIPTION', '')
    if not desc:
        return
    if cmd == 'python':
        Path('script.py').write_text('print("Hello!")\n')
    for line in desc.splitlines():
        line = line.strip()
        if line.startswith('$ cg '):
            with capsys.disabled():
                print(line)
            line = line.replace('cg acwf', 'cg acwf -memt')
            line = line[5:]
            line1, _, line2 = line.partition(' | cg ')
            if line2:
                err = cg(line1)
                assert not err
                captured = capsys.readouterr()
                monkeypatch.setattr('sys.stdin', io.StringIO(captured.out))
                line1 = line2
            with capsys.disabled():
                err = cg(line1)
                assert not err
