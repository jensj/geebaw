import json
import os
from pathlib import Path

import pytest
from ase import Atoms

from cligpaw.cli import main as cg


def test_emt(tmp_path):
    os.chdir(tmp_path)
    cg('build fcc:Cu,C cu.xyz')
    cg('relax -memt -S0.01 cu.xyz')
    cg('eos relax.traj -memt')
    dct = json.loads(Path('eos.json').read_text())
    assert dct['energy'] == pytest.approx(dct['bm_energy'], abs=1e-5)
    assert dct['volume'] == pytest.approx(dct['bm_volume'])
    assert dct['B'] == pytest.approx(dct['bm_B'], abs=0.001)


def test_gpaw(tmp_path):
    os.chdir(tmp_path)
    cg('build -d0.75 dimer:H h2.xyz')
    cg('relax h2.xyz -F0.05')


def test_gpaw_1d(tmp_path, monkeypatch):
    import matplotlib.pyplot as plt
    os.chdir(tmp_path)
    h = Atoms('H', cell=[4, 4, 1], pbc=(0, 0, 1))
    h.center()
    h.write('h.xyz')
    cg('relax h.xyz -S0.01')
    cg('scf relax.traj -G h.gpw')
    cg('fixed-density h.gpw -k GX:11')
    monkeypatch.setattr(plt, 'show', lambda: None)
    cg('band-structure h.gpw -k GX:3')
    cg('band-structure fixed-density.gpw')


def test_build():
    cg('build diamond:C')
