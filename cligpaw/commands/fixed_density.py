from cligpaw.commands.params import create_parameters
from cligpaw.commands.command import add_common_args

DESCRIPTION = """Do a fixed-density calculation.

More bands and more k-points:

  $ cg build diamond:Si,a=5.4 | cg scf -G si1.gpw
  $ cg fixed-density si1.gpw -k 8,8,8,G -p nbands=20 -G si2.gpw
"""


def init(parser):
    parser = parser.add_parser('fixed-density', description=DESCRIPTION)
    parser.add_argument('gpw_file')
    add_common_args(parser, 'Bnx', gpaw=True, params=True)
    return parser


def run(args, ctx):
    from gpaw import GPAW
    calc = GPAW(args.gpw_file)
    name = args.name or 'fixed-density'
    params = create_parameters(args)
    if args.kpts:
        if 'path' in params['kpts']:
            params['symmetry'] = 'off'
    else:
        del params['kpts']
    del params['mode']
    del params['xc']
    params |= {'txt': f'{name}.txt'}
    fd = calc.fixed_density(**params)
    return {}, fd.get_atoms()
