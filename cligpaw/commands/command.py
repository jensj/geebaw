import sys

from ase import Atoms
from ase.io import read


def add_common_args(parser, args='', params=False, gpaw=False, atoms=False):
    add = parser.add_argument
    if 'v' in args:
        add('-v', '--verbose', action='store_true', help='Verbose output.')
    if 'q' in args:
        add('-q', '--quiet', action='store_true',
            help='Less output.')
    if 'B' in args:
        add('-B', '--progress-bar', action='store_true',
            help='Show a progress bar.')
    if 'n' in args:
        add('-n', '--name', default='',
            help='Name of filenames.')
    if 'x' in args:
        add('-x', '--args', metavar='PATH', help='Replace "-x path" with '
            'options read from file given by path or path/params.cg if '
            'path is a folder.')

    if params:
        add('-k', '--kpts',
            help='Number of k-points.  Can be three integers '
            '(giving the size of the Monkhorst-Pack grid directly) or a '
            'floating point number (a distance in Å), where the grid size '
            'will be chosen so that the size of the corresponding '
            'supercell will be at least that distance. '
            'Examples: "-k1,1,1" (Γ-point only), '
            '"-k6,6,6" (a 6x6x6 MP-grid), "-k 15.0".')
        add('-m', '--mode', help='Calculation mode: "pw", "lcao", "fd" '
            'or "emt".  Default is PW-mode.')
        add('-H', '--hubbard', help='Add Hubbard-U correction.')
        add('-e', '--ecut', type=float, help='Plave-wave cutoff energy in eV.')
        add('-s', '--smearing',
            help='Examples: "fd:0.1" is Fermi-dirac distribution with '
            'kT=0.1 eV. '
            'The default is "cs:0.2": cold-smearing (Marzari-Vanderbilt) '
            'with a width of 0.2 eV.')
        add('--dftd3', action='store_true', help='Use DFTD3 correction.')
        add('-p', '--parameters', action='append', default=[],
            help='Comma-separated key=value pairs of ' +
            'calculator specific parameters.')
    if gpaw:
        add('--profile', action='store_true', help='Run through cProfile '
            'module and produce "<name>-<rank>.pstat" file(s).')
        add('-P', '--parallel', type=int, default=1, metavar='NCORES',
            help='Run in parallel on NCORES cores.')
        add('--use-old-gpaw', action='store_true', help='Use legacy GPAW.')
        add('--debug', action='store_true',
            help='Run in debug-mode.')
        add('-G', '--gpw',
            help='Write gpw-file when done.  Use -G <name>.gpw+ to '
            'also include the wave functions.')
        add('--pot-path', help='Path to PAW-potentials.')
        add('--after', help='Perform operation after calculation.  ' +
            'Example: --after="atoms.calc.write(...)"')

    if atoms:
        add('atoms', nargs='?', default='-',
            help='File to read atoms from.  Default is to '
            'read from stdin in xyz-format.')


def read_atoms(args) -> Atoms:
    if args.atoms == '-':
        if sys.stdin.isatty():
            raise FileNotFoundError(
                'Failed reading xyz-formatted atoms from stdin')
        atoms = read(sys.stdin, format='extxyz')
    else:
        atoms = read(args.atoms)
    assert isinstance(atoms, Atoms)
    return atoms
