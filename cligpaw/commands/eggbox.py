import numpy as np
from ase.units import Bohr
from cligpaw.calculator import attach_calculator
from cligpaw.commands.command import add_common_args, read_atoms
from cligpaw.commands.params import create_parameters

DESCRIPTION = """Egg-box error test.

Example:

  $ cg build atom:Li --pbc=1 -V 2.0 li.xyz
  $ cg eggbox li.xyz --npoints=21
"""


def init(parser):
    parser = parser.add_parser('eggbox', description=DESCRIPTION)
    add = parser.add_argument
    add('--npoints', type=int, default=31,
        help='Number of points (defaults to 31).')
    add_common_args(parser, 'Bnxv', gpaw=True, params=True, atoms=True)
    return parser


def run(args, ctx):
    atoms = read_atoms(args)
    params = create_parameters(args)
    params['txt'] = (args.name or 'eggbox') + '.txt'
    params['symmetry'] = {'point_group': False}
    if 'convergence' not in params:
        params['convergence'] = {'density': 1e-6}
    attach_calculator(atoms, params, args)
    energies = []
    displacements = [0.0]
    for i in range(args.npoints):
        ctx.update(completed=i / args.npoints)
        e = atoms.get_potential_energy()
        energies.append(e)
        if i == 0:
            grid = atoms.calc.dft.density.nt_sR.desc
            d = grid.cell_cv[0] / grid.size[0] / (args.npoints - 1) * Bohr
        if i < args.npoints - 1:
            atoms.positions += d
            displacements.append(displacements[-1] + np.linalg.norm(d))
    ctx.update(completed=1.0)
    data = dict(displacements=displacements,
                energies=energies,
                error=np.ptp(energies))
    return data, atoms
