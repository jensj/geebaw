from collections import defaultdict

from ase.dft.kpoints import monkhorst_pack_interpolate
from ase.units import Ha
from cligpaw.commands.params import parse_kpts_string
from cligpaw.commands.command import add_common_args

DESCRIPTION = """Plot band-structure.

Read eigenvalues and k-points from file and plot result from
band-structure calculation or interpolate
from Monkhorst-Pack sampling to a given path (--kpts <path>).

Example:

  $ cg build fcc:Al | cg scf -G al.gpw
  $ cg fixed-density al.gpw -k GX -G al-bs.gpw
  $ cg band-structure al-bs.gpw -r -10 10
"""


def init(parser):
    parser = parser.add_parser(
        'band-structure',
        description=DESCRIPTION)
    add_common_args(parser, 'qBn')
    add = parser.add_argument
    add('gpw_file',
        help='Path to output file(s) from calculation.')
    add('-k', '--kpts', help='Example "GXL".')
    add('-r', '--range', nargs=2, type=float, metavar=('EMIN', 'EMAX'),
        help='Example: "-r -3.0 3.0" (in eV relative to Fermi-level).')
    return parser


def run(args, ctx):
    import matplotlib.pyplot as plt
    from gpaw.new.brillouin import MonkhorstPackKPoints
    from gpaw.new.gpw import read_gpw

    atoms, dft, params, _ = read_gpw(args.gpw_file)
    ibzwfs = dft.ibzwfs
    (fermilevel,) = ibzwfs.fermi_levels * Ha
    eig_skn, _ = ibzwfs.get_all_eigs_and_occs()
    eig_skn *= Ha
    if not args.quiet:
        print('Spins, k-points, bands: {}, {}, {}'.format(*eig_skn.shape))
    ibz = ibzwfs.ibz
    bz = ibz.bz
    if isinstance(bz, MonkhorstPackKPoints):
        if args.kpts is None:
            raise ValueError('Please specify a path!')
        path = parse_kpts_string(args.kpts)
        if not args.quiet:
            print('Interpolating from Monkhorst-Pack grid (size, offset):')
        path = atoms.cell.bandpath(pbc=atoms.pbc, **path)
        path_kpts = path.kpts
        icell = atoms.cell.reciprocal()
        eig_skn = monkhorst_pack_interpolate(path_kpts,
                                             eig_skn.transpose(1, 0, 2),
                                             icell, ibz.bz2ibz_K, bz.size_c,
                                             bz.shift_c)
        eig_skn = eig_skn.transpose(1, 0, 2)
    else:
        path = atoms.cell.bandpath(pbc=atoms.pbc, **params.kpts)
        if args.kpts is not None:
            raise ValueError('Path already given:', path)

    x, xlabels, labels = path.get_linear_kpoint_axis()
    labels = [r'$\Gamma$' if L == 'G' else
              (L if len(L) == 1 else f'${L[0]}$_{L[1]}$')
              for L in labels]
    dd = defaultdict(list)
    for xlabel, label in zip(xlabels, labels):
        dd[xlabel].append(label)
    xlabels = list(dd)
    labels = [','.join(val) for val in dd.values()]
    ax = plt.figure().add_subplot(111)
    for y in eig_skn[0].T:
        ax.plot(x, y, color='g')
    for x0 in xlabels[1:-1]:
        ax.axvline(x0, color='0.5')
    ax.set_xticks(xlabels)
    ax.set_xticklabels(labels)
    ax.set_ylabel('eigenvalues [eV]')
    ax.axhline(fermilevel, color='0.5',
               label=f'Fermi-level: {fermilevel:.3f} eV')

    if args.range:
        emin, emax = (fermilevel + e for e in args.range)
    else:
        emin = eig_skn.min() - 1
        emax = eig_skn.max() + 1
    ax.axis(xmin=0, xmax=x[-1], ymin=emin, ymax=emax)
    plt.legend()
    plt.show()
    return {}, atoms
