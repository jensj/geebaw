import sys
from pathlib import Path


def init(parser):
    parser = parser.add_parser(
        'completion',
        description="""Enable BASH tab-completion.

Do this:

  $ cg completion >> ~/.bashrc
""")
    return parser


def completion_command() -> str:
    py = sys.executable
    filename = Path(__file__).parent.parent / 'complete.py'
    return f'complete -o default -C "{py} {filename}" cg'


def run(args):
    cmd = completion_command()
    if args.verbose:
        print('Add tab-completion for Bash by copying the following '
              f'line to your ~/.bashrc (or similar file):\n\n   {cmd}\n')
    else:
        print(cmd)
    return {}, None
