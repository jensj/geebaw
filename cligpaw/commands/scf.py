from math import log10

import numpy as np
from gpaw.mpi import broadcast_float

from cligpaw.calculator import attach_calculator
from cligpaw.commands.command import add_common_args, read_atoms
from cligpaw.commands.params import create_parameters

DESCRIPTION = """Do a scf calculation.

Example:

Calculate energy and forces for H2-molecule and write result to
h2.txt and h2.json files:

  $ cg build mol:H2 | cg scf -n h2 --properties=f
"""


def init(parser):
    parser = parser.add_parser('scf', description=DESCRIPTION)
    add_common_args(parser, 'vBnx', gpaw=True, params=True, atoms=True)
    parser.add_argument(
        '--properties', default='',
        help='Calculate extra properties '
        '(default is to calculate the energy only).  Use "f" for forces, '
        '"s" for stress tensor and "fs" for both.')
    return parser


def run(args, ctx):
    for p in args.properties:
        if p not in 'fs':
            raise ValueError(f'Unknown property: {p}')
    atoms = read_atoms(args)
    params = create_parameters(args)
    if 'txt' not in params:
        params['txt'] = ('-' if args.name == '-' else
                         (args.name or 'scf') + '.txt')
    attach_calculator(atoms, params, args)
    if args.progress_bar:
        atoms.calc.hooks['scf_step'] = SCFProgress(atoms.calc, ctx)
    results = {'energy': atoms.get_potential_energy()}
    if 'f' in args.properties:
        results['forces'] = atoms.get_forces()
    if 's' in args.properties:
        results['stress'] = atoms.get_stress()
    return results, atoms


class SCFProgress:
    def __init__(self, calc, progress):
        self.calc = calc
        self.progress = progress
        self.x0 = {}

    def __call__(self, ctx):
        criteria = self.calc.dft.scf_loop.convergence
        self.progress.update(completed=self.completed(criteria, ctx))

    def completed(self, criteria, ctx):
        completed = 1.0
        override = 0.1
        for name, criterion in criteria.items():
            if not criterion.calc_last:
                c = self.check(criterion, ctx)
                if criterion.override_others:
                    if c > 0.0:
                        override = c
                else:
                    completed = min(completed, c)

        for name, criterion in criteria.items():
            if criterion.calc_last:
                if completed == 1.0:
                    completed = self.check(criterion, ctx)

        return max(completed, override)

    def check(self, criterion, ctx):
        func = globals().get(criterion.name)
        if func is None:
            return 1.0
        x, x1 = func(criterion, ctx)
        x0 = self.x0.get(criterion.name)
        if x0 is None:
            if x is None:
                return 0.0
            self.x0[criterion.name] = x
            return 0.0
        if x is None or x > x0:
            return 0.0
        if x < x1:
            return 1.0
        return log10(x0 / x) / log10(x0 / x1)


def energy(crit, context):
    if len(crit._old) < crit._old.maxlen:
        return None, crit.tol
    error = np.ptp(crit._old)
    return error, crit.tol


def density(crit, context):
    if context.dens.fixed:
        return 0.0, 1.0
    nv = context.wfs.nvalence
    if nv == 0:
        return 0.0, 1.0
    # Make sure all agree on the density error.
    error = broadcast_float(context.dens.error, context.wfs.world) / nv
    if np.isfinite(error):
        return error, crit.tol
    return None, crit.tol


def eigenstates(crit, context):
    if context.wfs.nvalence == 0:
        return 0.0, 1.0
    error = crit.get_error(context)
    if np.isfinite(error):
        return error, crit.tol
    return None, crit.tol


def forces(crit, context):
    if np.isinf(crit.atol) and np.isinf(crit.rtol):
        return 0.0, 1.0
    if not hasattr(crit, 'old_old_F_av'):
        crit.old_old_F_av = crit.old_F_av
        return None, 1.0
    F_av = crit.old_F_av
    max_force = np.max(np.linalg.norm(F_av, axis=1))
    error = ((F_av - crit.old_old_F_av)**2).sum(1).max()**0.5

    if np.isfinite(crit.rtol):
        error_threshold = min(crit.atol, crit.rtol * max_force)
    else:
        error_threshold = crit.atol
    return error, error_threshold
