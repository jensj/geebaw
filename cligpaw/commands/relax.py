from math import inf

from cligpaw.calculator import attach_calculator
from cligpaw.commands.command import add_common_args, read_atoms
from cligpaw.commands.params import create_parameters
from cligpaw.relax import relax

DESCRIPTION = """Relax atoms and/or unit cell.

Example:

  $ cg build diamond:Si,a=5.5 | cg relax -S 0.01
"""


def init(parser):
    parser = parser.add_parser('relax', description=DESCRIPTION)
    add = parser.add_argument
    add('-F', '--maximum-force', type=float, default=0.05,
        help='Relax internal coordinates.')
    add('-S', '--maximum-stress', type=float, default=inf,
        help='Relax unit-cell and internal coordinates.')
    add('--smask', help='Ignore stress tensor components '
        '(xx, yy, zz, zy, zx, yx).  Examples: '
        '"--smask=111000" (ignore shear), '
        '"--smask=110001" (slab).')
    add_common_args(parser, 'Bnx', gpaw=True, params=True, atoms=True)
    return parser


def run(args, context):
    atoms = read_atoms(args)
    params = create_parameters(args)
    if 'txt' not in params:
        params['txt'] = (args.name or 'relax') + '.txt'
    if 'convergence' not in params:
        params['convergence'] = {'density': 1e-6}
    attach_calculator(atoms, params, args)
    context.update(total=1.0)
    data = relax(
        atoms,
        fmax=args.maximum_force,
        smax=args.maximum_stress,
        smask=args.smask and [int(s) for s in args.smask],
        name=args.name,
        callback=lambda completed: context.update(completed=completed))
    return data, atoms
