import importlib
import sys
from typing import Callable

import ase.build as build
import numpy as np
from ase import Atoms
from ase.data import atomic_numbers, covalent_radii
from ase.io import read, write
from ase.symbols import string2symbols

_functions: dict[str, Callable] = {}


def description():
    desc = 'Build atomic-structure.\n\nRecipes:\n\n'
    for name, func in _functions.items():
        args = ','.join(func.__code__.co_varnames[:func.__code__.co_argcount])
        desc += f'* {name}:{args}\n'
    return desc


def build_function(f):
    _functions[f.__name__] = f
    return f


def str2obj(s: str) -> bool | int | float | str:
    x = {'True': True, 'False': False}.get(s)
    if x:
        return x
    for type in [int, float]:
        try:
            return type(s)
        except ValueError:
            pass
    return s


def init(parser):
    parser = parser.add_parser(
        'build',
        description=description())
    add = parser.add_argument
    add('--pbc', help='Set periodic boundary conditions. Examples: '
        '"--pbc 1,1,0" (slab), "--pbc=0" (molecule).')
    add('-M', '--magmoms',
        help='Magnetic moments.  '
        'Use "-M 1" or "-M 2.3,-2.3"')
    add('-V', '--vacuum', type=float, default=3.0,
        help='Amount of vacuum to add along non-periodic direction(s) '
        '(in Å)')
    add('-r', '--repeat',
        help='Repeat unit cell.  Use "-r 2" or "-r 2,3,1"')
    add('-d', '--bond-length', type=float, metavar='LENGTH',
        help='Bond length in Å')
    recipes = '", "'.join(_functions)
    add('atoms', help='Name of a file to read atoms from or a '
        'build-recipe like "recipe:args", where "recipe" must be one of '
        f'"{recipes}" and "args" are arguments to the recipe. '
        'Examples: "mol:H2O", "bcc:Fe", "diamond:Si,a=5.4".')
    add('output', nargs='?', default='-', help='Output file '
        '(defaults to stdout).  Examples: "abc.xyz", "x.traj".')
    return parser


def run(args, ctx):
    name, sep, extra = args.atoms.partition(':')
    if sep == ':' and (name in _functions or '.' in name):
        if name in _functions:
            func = _functions[name]
        else:
            module, name = name.rsplit('.', 1)
            func = getattr(importlib.import_module(module), name)
        args_ = []
        kwargs = {}
        for arg in extra.split(','):
            if '=' in arg:
                k, v = arg.split('=')
                kwargs[k] = str2obj(v)
            else:
                args_.append(str2obj(arg))
        atoms, d = func(*args_, **kwargs)
    else:
        atoms = read(args.atoms)
        d = 0.0

    if args.magmoms:
        if ':' in args.magmoms:
            for x in args.magmoms.split(','):
                symbol, m = x.split(':')
                for atom in atoms:
                    if atom.symbol == symbol:
                        atom.magmom = float(m)
        else:
            magmoms = np.array(
                [float(m) for m in args.magmoms.split(',')])
            atoms.set_initial_magnetic_moments(
                np.tile(magmoms, len(atoms) // len(magmoms)))
        for name in ['magmoms', 'magmom']:
            try:
                atoms.calc.results.pop(name, None)
            except AttributeError:
                break

    if args.bond_length and d > 0.0:
        if atoms.pbc.any():
            e_cc = np.diag([args.bond_length / d if periodic else 1.0
                            for periodic in atoms.pbc])
            atoms.set_cell(e_cc @ atoms.cell, scale_atoms=True)
        else:
            atoms.positions *= args.bond_length / d

    if args.repeat is not None:
        r = args.repeat.split(',')
        if len(r) == 1:
            r = 3 * r
        atoms = atoms.repeat([int(c) for c in r])

    if not atoms.pbc.all():
        atoms.center(axis=[i for i, periodic in enumerate(atoms.pbc)
                           if not periodic],
                     vacuum=args.vacuum)

    if args.pbc:
        pbc = [int(p) for p in args.pbc.split(',')]
        if len(pbc) == 1:
            pbc = pbc * 3
        atoms.pbc = pbc

    if args.output == '-':
        write(sys.stdout, atoms, format='extxyz')
    else:
        write(args.output, atoms)

    return {}, atoms


def radius(symbol: str) -> float:
    # convert np.float64 to float (looks nicer):
    return float(covalent_radii[atomic_numbers[symbol]])


@build_function
def mol(name) -> tuple[Atoms, float]:
    atoms = build.molecule(name)
    return atoms, 0.0


@build_function
def atom(symbol) -> tuple[Atoms, float]:
    return Atoms(symbol), 0.0


@build_function
def dimer(name) -> tuple[Atoms, float]:
    symbols = string2symbols(name)
    if len(symbols) == 1:
        symbols = symbols * 2
    else:
        assert len(symbols) == 2
    d = radius(symbols[0]) + radius(symbols[1])
    return Atoms(symbols, positions=[(0, 0, 0), (d, 0, 0)]), d


@build_function
def fcc(symbol: str,
        celltype: str = 'P',
        a: float | None = None) -> tuple[Atoms, float]:
    """FCC crystal.

    >>> atoms, d = fcc('Cu')
    >>> len(atoms), d
    (1, 3.036)
    >>> atoms, d = fcc('Cu', 'C')
    >>> len(atoms), d
    (4, 3.036)
    """
    if a is None:
        d = radius(symbol) * 2.3
        a = 2**0.5 * d
    else:
        d = a / 2**0.5
    atoms = build.bulk(symbol,
                       crystalstructure='fcc',
                       a=a,
                       cubic=celltype == 'C')
    return atoms, d


@build_function
def bcc(symbol: str,
        celltype: str = 'P',
        a: float | None = None) -> tuple[Atoms, float]:
    """BCC crystal.

    >>> atoms, d = bcc('W')
    >>> len(atoms), d
    (1, 3.24)
    >>> atoms, d = bcc('W', 'C')
    >>> len(atoms), d
    (2, 3.24)
    """
    if a is None:
        d = radius(symbol) * 2.0
        a = 2 * d / 3**0.5
    else:
        d = 0.5 * a * 3**0.5
    atoms = build.bulk(symbol,
                       crystalstructure='bcc',
                       a=a,
                       cubic=celltype == 'C')
    return atoms, d


@build_function
def diamond(symbol: str,
            celltype: str = 'P',
            a: float | None = None) -> tuple[Atoms, float]:
    if a is None:
        d = radius(symbol) * 2.0
        a = 4 * d / 3**0.5
    else:
        d = 3**0.5 * a / 4
    atoms = build.bulk(symbol,
                       crystalstructure='diamond',
                       a=a,
                       cubic=celltype == 'C')
    return atoms, d


@build_function
def zincblende(symbols: str,
               celltype: str = 'P',
               a: float | None = None) -> tuple[Atoms, float]:
    symbol1, symbol2 = string2symbols(symbols)
    if a is None:
        d = radius(symbol1) + radius(symbol2)
        a = 4 * d / 3**0.5
    else:
        d = 3**0.5 * a / 4
    atoms = build.bulk(symbols,
                       crystalstructure='zincblende',
                       a=a,
                       cubic=celltype == 'C')
    return atoms, d


@build_function
def mx2(formula: str,
        kind: str = '2H',
        a: float | None = None,
        thickness: float | None = None):
    s1, s2, s3 = string2symbols(formula)
    assert s2 == s3
    if a is None:
        a = radius(s1) * 2.06
    if thickness is None:
        d = (radius(s1) + radius(s2)) * 0.94
        thickness = 2 * (d**2 - a**2 / 3)**0.5
    return build.mx2(formula, kind, a, thickness), a


@build_function
def graphene(formula: str,
             a: float | None = None):
    s1, s2 = string2symbols(formula)
    if a is None:
        a = (radius(s1) + radius(s2)) * 3**0.5 * 0.934
    return build.graphene(formula, a), a / 3**0.5
