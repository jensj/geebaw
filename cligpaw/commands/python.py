import runpy
import sys

from cligpaw.commands.command import add_common_args

DESCRIPTION = """Run Python interpreter.

Instead of "python script.py" you can do:

  $ cg python --progress --pot-path=. script.py
"""


def init(parser):
    parser = parser.add_parser('python', description=DESCRIPTION)
    add_common_args(parser, 'B', gpaw=True)
    add = parser.add_argument
    add('--command', '-c',
        dest='cmd',
        nargs=1,  # argparse.REMAINDER,
        help='Program passed in as string (terminates option list).')
    add('--module', '-m',
        nargs=1,  # argparse.REMAINDER,
        help='Run library module as a script (terminates option list).')
    add('arguments', metavar='ARG',
        help='Arguments passed to program in '
        'sys.argv[1:].  '
        'Use -- to force all remaining arguments to be '
        'passed to target program (useful for passing '
        'options starting with -).',
        nargs='*')
    return parser


def run(args, ctx):
    if args.progress_bar:
        import gpaw.new.ase_interface as ase_interface
        from cligpaw.commands.scf import SCFProgress
        GPAW = ase_interface.GPAW

        def GPAW2(*args, **kwargs):
            calc = GPAW(*args, **kwargs)
            calc.hooks['scf_step'] = SCFProgress(calc, ctx)
            return calc

        ase_interface.GPAW = GPAW2

    if args.cmd:
        sys.argv[:] = ['-c'] + args.cmd[1:]
        d = {}
        exec(args.cmd[0], d, d)
    elif args.module:
        sys.argv[:] = args.module
        runpy.run_module(args.module[0],
                         run_name='__main__',
                         alter_sys=True)
    else:
        sys.argv[:] = args.arguments
        runpy.run_path(args.arguments[0], run_name='__main__')

    return {}, None
