from cligpaw.bader import bader

DESCRIPTION = """Bader analysis.

...
"""


def init(parser):
    parser = parser.add_parser('bader', description=DESCRIPTION)
    parser.add_argument('gpw_file')
    return parser


def run(args, ctx):
    from gpaw import GPAW
    atoms = GPAW(args.gpw_file).get_atoms()
    charges = bader(atoms)
    return {'charges': charges.tolist()}, atoms
