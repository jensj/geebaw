import argparse
from math import pi
from pprint import pprint
from typing import Any


def parse_kpts_string(kpts: str) -> dict:
    if kpts[0].isalpha():
        path, _, n = kpts.partition(':')
        if n:
            npoints = int(n)
        else:
            npoints = 6 * (len(path) - 1) + 1
        return {'path': path, 'npoints': npoints}
    dct = {}
    if kpts.endswith(',G'):
        dct['gamma'] = True
        kpts = kpts[:-2]
    if ',' in kpts:
        return dct | {'size': [int(k) for k in kpts.split(',')]}
    return dct | {'density': float(kpts) / (2 * pi)}


def create_parameters(args: argparse.Namespace,
                      mode='pw',
                      ecut=400.0,
                      kpts='10.0',
                      smearing='cs:0.2',
                      xc='PBE') -> dict[str, Any]:
    from gpaw.mpi import world
    params: dict[str, Any] = {}
    if args.ecut and args.mode != 'emt':
        params['mode'] = {'name': 'pw', 'ecut': args.ecut}
    elif args.mode:
        params['mode'] = {'name': args.mode}
        if args.mode == 'pw':
            params['mode']['ecut'] = ecut
        elif args.mode == 'lcao':
            params['basis'] = 'dzp'
    else:
        params['mode'] = {'name': 'pw', 'ecut': ecut}
    params['kpts'] = parse_kpts_string(args.kpts or kpts)
    name, width = (args.smearing or smearing).split(':')
    name = {'cs': 'marzari-vanderbilt',
            'mv': 'marzari-vanderbilt',
            'fd': 'fermi-dirac'}[name]
    params['occupations'] = {'name': name, 'width': float(width)}
    params['xc'] = xc

    for p in args.parameters:
        params.update(str2dict(p))

    if world.rank == 0:
        pprint(params)

    return params


def str2dict(s: str, namespace={}, sep: str = '=') -> dict[str, Any]:
    """Convert comma-separated key=value string to dictionary.

    Examples:

    >>> str2dict('xc=PBE,nbands=200,parallel={band:4}')
    {'xc': 'PBE', 'nbands': 200, 'parallel': {'band': 4}}
    >>> str2dict('a=1.2,b=True,c=ab,d=1,2,3,e={f:42,g:cd}')
    {'a': 1.2, 'b': True, 'c': 'ab', 'd': (1, 2, 3), 'e': {'f': 42, 'g': 'cd'}}
    """

    def myeval(value):
        try:
            value = eval(value, namespace)
        except (NameError, SyntaxError):
            pass
        return value

    assert sep in s
    dct = {}
    strings = (s + ',').split(sep)
    for i in range(len(strings) - 1):
        key = strings[i]
        m = strings[i + 1].rfind(',')
        value: Any = strings[i + 1][:m]
        if value[0] == '{':
            assert value[-1] == '}'
            value = str2dict(value[1:-1], namespace, ':')
        elif value[0] == '(':
            assert value[-1] == ')'
            value = [myeval(t) for t in value[1:-1].split(',')]
        else:
            value = myeval(value)
        dct[key] = value
        strings[i + 1] = strings[i + 1][m + 1:]
    return dct
