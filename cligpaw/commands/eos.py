from cligpaw.commands.params import create_parameters
from cligpaw.commands.command import add_common_args, read_atoms
from cligpaw.calculator import attach_calculator
from cligpaw.eos import eos

DESCRIPTION = """Equation of state fit.

Uses Birch-Murnaghan equation.  Example:

  $ cg build fcc:Al al.xyz
  $ cg eos al.xyz -m emt --max-strain=0.02 --npoints=5 -v
"""


def init(parser):
    parser = parser.add_parser('eos', description=DESCRIPTION)
    add = parser.add_argument
    add('--npoints', type=int, default=7,
        help='Number of points (defaults to 7).')
    add('--max-strain', type=float, default=0.02,
        help='Maximum strain.  Default is "--max-strain=0.02" meaning that '
        'lattice vectors are varied from -2 to +2 %%.')
    add_common_args(parser, 'Bnxv', gpaw=True, params=True, atoms=True)
    return parser


def run(args, ctx):
    atoms = read_atoms(args)
    params = create_parameters(args)
    params['txt'] = (args.name or 'eos') + '.txt'
    if 'convergence' not in params:
        params['convergence'] = {'density': 1e-6}
    attach_calculator(atoms, params, args)
    data = eos(atoms, args.npoints, args.max_strain,
               callback=lambda completed: ctx.update(completed=completed))
    return data, atoms
