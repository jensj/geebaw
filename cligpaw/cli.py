from __future__ import annotations

import argparse
import cProfile
import importlib
import json
import os
import subprocess
import sys
from contextlib import contextmanager
from pathlib import Path
from time import time

import numpy as np

from cligpaw.argparse2 import MyArgumentParser
from cligpaw.completion import ensure_completions

COMMAND_NAMES = [
    'build',
    'scf', 'relax', 'eos', 'eggbox',
    'bader',
    'fixed_density',
    'band_structure',
    'python',
    'completion', 'acwf']


def main(argv: str | list[str] | None = None) -> int:
    ensure_completions()
    parser = MyArgumentParser(
        prog='cg',
        description='Command-line interface for GPAW')

    commands = {}
    for name in COMMAND_NAMES:
        module = importlib.import_module(f'cligpaw.commands.{name}')
        module.init(parser)
        commands[name.replace('_', '-')] = module.run

    args = parser.parse_args(argv)

    if not args.command:
        parser.parser.print_help()
        return 0

    if args.command == 'help':
        if args.cmd is None:
            parser.parser.print_help()
        else:
            parser.subparsers.choices[args.cmd.replace('_', '-')].print_help()
        return 0

    from gpaw.mpi import world
    if getattr(args, 'parallel', 1) > 1:
        if world.size == 1:
            # Start again in parallel:
            argv = (['mpiexec', '-np', str(args.parallel)] +
                    os.environ.get('GPAW_MPI_OPTIONS', '').split() +
                    [sys.executable, '-m', 'cligpaw.cli'] +
                    sys.argv[1:])
            # Use a clean set of environment variables without any MPI stuff:
            subprocess.run(argv, check=True, env=os.environ)
            return 0

    run = commands[args.command]

    args.name = getattr(args, 'name', '')

    if getattr(args, 'profile', False):
        with cProfile.Profile() as prof:
            run_cmd(run, args)
        if args.name == '-':
            if world.rank == 0:
                prof.print_stats('time')
        else:
            name = args.name or 'prof'
            prof.dump_stats(f'{name}-{world.rank:03}.pstats')
    else:
        try:
            run_cmd(run, args)
        except Exception:
            if world.rank == 0:
                from rich.console import Console
                console = Console(stderr=True)
                console.print_exception()
            return 1

    return 0


def ndarray2list(obj: np.ndarray) -> list:
    if isinstance(obj, np.ndarray):
        return obj.tolist()
    raise TypeError


@contextmanager
def context(add_progress_bar: bool):
    if add_progress_bar:
        from rich.progress import Progress
        with Progress() as progress:
            yield Context(progress)
    else:
        yield EmptyContext()


class EmptyContext:
    def update(self, **kwargs):
        pass


class Context(EmptyContext):
    def __init__(self, progress):
        self.progress = progress
        self.id = progress.add_task('Running', total=1.0)

    def update(self, **kwargs):
        self.progress.update(self.id, **kwargs)


def run_cmd(run,
            args: argparse.Namespace) -> None:
    from gpaw.mpi import world
    t1 = time()
    with context(getattr(args, 'progress_bar', False) and
                 world.rank == 0) as ctx:
        results, atoms = run(args, ctx)
    after(atoms, args)
    t2 = time()
    results['time'] = t2 - t1
    if world.rank == 0 and len(results) > 1:
        p = Path((args.name or args.command) + '.json')
        p.write_text(json.dumps(results, indent=2, default=ndarray2list))
        if getattr(args, 'verbose', False):
            print(results)


def after(atoms, args):
    if getattr(args, 'after', ''):
        exec(args.after, {'atoms': atoms})
    if getattr(args, 'gpw', ''):
        mode = 'all' if args.gpw.endswith('+') else None
        atoms.calc.write(args.gpw.rstrip('+'), mode=mode)


if __name__ == '__main__':
    sys.exit(main())
