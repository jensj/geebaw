from typing import Callable

import numpy as np
from ase import Atoms
from ase.calculators.calculator import PropertyNotImplementedError
from ase.eos import EquationOfState


def eos(atoms: Atoms,
        npoints: int = 7,
        maxstrain: float = 0.02,
        callback: Callable[[float], None] = lambda x: None) -> dict:
    ndims = atoms.pbc.sum()
    assert ndims > 0

    fix_gpts_and_kpts(atoms)

    cell_cv = atoms.get_cell()

    energies = []
    magmoms = []
    strains = np.linspace(-maxstrain, maxstrain, npoints)
    for i, strain in enumerate(strains):
        callback(i / npoints)
        e_cc = np.diag([1.0 + strain if periodic else 1.0
                        for periodic in atoms.pbc])
        atoms.set_cell(e_cc @ cell_cv, scale_atoms=True)
        e = atoms.get_potential_energy()
        energies.append(e)
        try:
            m = atoms.get_magnetic_moment()
        except PropertyNotImplementedError:
            pass
        else:
            magmoms.append(m)

    callback(1.0)

    strain, e0, d2eds2 = fit(strains, energies)

    results = dict(
        strains=strains.tolist(),
        energies=energies,
        energy=e0,
        strain=strain,
        d2eds2=d2eds2)
    if magmoms:
        results.update(magmoms=magmoms)

    v0 = generalized_volume(cell_cv[atoms.pbc])
    volume = v0 * (1 + strain)**ndims
    name = ['length', 'area', 'volume'][ndims - 1]
    results[name] = volume

    if ndims == 3:
        results['B'] = d2eds2 * (1 + strain)**2 / 9 / volume
        volumes = v0 * (1.0 + strains)**3
        eos = EquationOfState(volumes, energies, 'birchmurnaghan')
        v0, e0, B = eos.fit()
        Bp = eos.eos_parameters[2]
        results.update(bm_volume=v0,
                       bm_energy=e0,
                       bm_B=B, bm_Bp=Bp)

    e_cc = np.diag([1.0 + strain if periodic else 1.0
                    for periodic in atoms.pbc])
    atoms.set_cell(e_cc @ cell_cv, scale_atoms=True)

    return results


def generalized_volume(cell_cv):
    if len(cell_cv) == 3:
        return abs(np.linalg.det(cell_cv))
    if len(cell_cv) == 2:
        return np.linalg.norm(np.cross(*cell_cv))
    return np.linalg.norm(cell_cv[0])


def fit(strains, energies):
    """
                   3
                  ---   n        1
    f(s) = p(x) = >  c x,  x = -----
                  --- n        1 + s
                  n=0

    df/ds = dp/dx dx/ds

    d2f/ds2 = d2pdx2 (dx/ds)^2

    """

    x = 1 / (1 + strains)
    p0 = np.poly1d(np.polyfit(x, energies, 3))
    p1 = np.polyder(p0, 1)
    p2 = np.polyder(p1, 1)
    for x0 in np.roots(p1):
        if isinstance(x0, float) and x0 > 0 and p2(x0) > 0:
            break
    else:  # no break
        raise ValueError
    strain = 1 / x0 - 1
    e0 = p0(x0)
    d2eds2 = p2(x0) * x0**2
    return strain, e0, d2eds2


def fix_gpts_and_kpts(atoms: Atoms) -> None:
    from gpaw.calculator import GPAW as OldGPAW
    from gpaw.new.ase_interface import GPAW as NewGPAW
    from gpaw.new.brillouin import MonkhorstPackKPoints
    from gpaw.new.builder import builder

    params = dict(getattr(atoms.calc, 'parameters').items())
    if 'mode' not in params:
        # Not a GPAW calculation
        return
    build = builder(atoms,
                    params={k: v for k, v in params.items()
                            if k in {'gpts', 'kpts', 'h', 'mode',
                                     'xc', 'setups'}})
    bz = build.ibz.bz
    assert isinstance(bz, MonkhorstPackKPoints)
    params['kpts'] = {'size': bz.size_c,
                      'gamma': (abs(bz.kpt_Kc).sum(axis=1) < 1e-12).any()}
    params['gpts'] = build.grid.size
    params['txt'] = atoms.calc.log.fd.name
    if atoms.calc.old:
        atoms.calc = OldGPAW(**params)
    else:
        atoms.calc = NewGPAW(**params)
